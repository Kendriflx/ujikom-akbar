-- phpMyAdmin SQL Dump
-- version 5.1.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Apr 14, 2022 at 03:18 AM
-- Server version: 10.4.18-MariaDB
-- PHP Version: 8.0.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_spp`
--

-- --------------------------------------------------------

--
-- Table structure for table `kelas`
--

CREATE TABLE `kelas` (
  `id_kelas` int(11) NOT NULL,
  `nama_kelas` varchar(10) NOT NULL,
  `kompetensi_keahlian` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kelas`
--

INSERT INTO `kelas` (`id_kelas`, `nama_kelas`, `kompetensi_keahlian`) VALUES
(1, 'XII RPL', 'RPL'),
(2, 'XII TKJ', 'TKJ'),
(3, 'XII OTKP', 'OTKP');

-- --------------------------------------------------------

--
-- Table structure for table `pembayaran`
--

CREATE TABLE `pembayaran` (
  `id_pembayaran` int(11) NOT NULL,
  `id_petugas` int(11) NOT NULL,
  `nisn` varchar(10) NOT NULL,
  `tgl_bayar` date NOT NULL,
  `bulan_dibayar` varchar(8) NOT NULL,
  `tahun_dibayar` varchar(4) NOT NULL,
  `id_spp` int(11) NOT NULL,
  `jumlah_bayar` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pembayaran`
--

INSERT INTO `pembayaran` (`id_pembayaran`, `id_petugas`, `nisn`, `tgl_bayar`, `bulan_dibayar`, `tahun_dibayar`, `id_spp`, `jumlah_bayar`) VALUES
(1, 1, '123456789', '2022-01-26', '1', '1', 1, 450000),
(2, 2, '456123789', '2022-01-26', '1', '1', 2, 500000),
(3, 3, '7894561230', '2022-01-26', '1', '1', 3, 400000);

-- --------------------------------------------------------

--
-- Table structure for table `petugas`
--

CREATE TABLE `petugas` (
  `id_petugas` int(11) NOT NULL,
  `username` varchar(25) NOT NULL,
  `password` varchar(32) NOT NULL,
  `nama_petugas` varchar(35) NOT NULL,
  `level` enum('admin','petugas') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `petugas`
--

INSERT INTO `petugas` (`id_petugas`, `username`, `password`, `nama_petugas`, `level`) VALUES
(1, 'jajang', 'jajang1', 'Jajang H', 'petugas'),
(2, 'CucepKece', 'akukece', 'Cucup', 'admin'),
(3, 'Ameen', 'amen', 'Amen', 'petugas');

-- --------------------------------------------------------

--
-- Table structure for table `siswa`
--

CREATE TABLE `siswa` (
  `nisn` char(10) NOT NULL,
  `nis` char(8) NOT NULL,
  `nama` varchar(35) NOT NULL,
  `id_kelas` int(11) NOT NULL,
  `alamat` text NOT NULL,
  `no_telp` varchar(13) NOT NULL,
  `id_spp` int(11) NOT NULL,
  `email` varchar(128) NOT NULL,
  `password` varchar(128) NOT NULL,
  `role_id` int(11) NOT NULL,
  `is_active` int(1) NOT NULL,
  `date_created` int(11) NOT NULL,
  `image` varchar(128) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `siswa`
--

INSERT INTO `siswa` (`nisn`, `nis`, `nama`, `id_kelas`, `alamat`, `no_telp`, `id_spp`, `email`, `password`, `role_id`, `is_active`, `date_created`, `image`) VALUES
('0042655511', '34234234', 'Joseph', 2, 'juji', '363454', 1, 'am@gmail.com', '$2y$10$SvYKnVv1yPAPcZhuzGiOo.yt.yaYl3rWd71RytynWZikUnKQce7bC', 2, 1, 1644467866, 'default.jpg'),
('123456789', '32145678', 'Kyura', 1, 'Jl.Leo No.3', '0895 4567 123', 2, '', '', 0, 0, 0, ''),
('1657995452', '78994523', 'Martu Ahmad', 2, 'jl orarara', '778462166', 2, 'martuahmad@gmail.com', '$2y$10$RKXgJW0m7j5ksELbUBKNLeVihll/4.xBqZljHa2axXN6VS.L8HSYK', 2, 1, 1648084316, 'default.jpg'),
('321542413', '21456124', 'Azril Dardi', 1, 'jl.pengkolan', '0895736183', 3, 'azril@gmail.com', '$2y$10$DkVYYCvPTHmxCt0RTc3eCO77ubdVsZQNezSwTTVTCNmCyg0WgAuK2', 2, 1, 1649292634, 'unnamed.png'),
('456123789', '12457896', 'Lymiura', 3, 'Jl.Baleendah', '08957894 565', 3, '', '', 0, 0, 0, ''),
('514263', '321456', 'Akbar Ramadhan', 2, 'gunung batu', '089594678945', 2, 'kendriflx@gmail.com', '$2y$10$7fhUYyUkWvXs2fpsq2h7M.dW4bEnCEH9glwnOs7F9rxCrXEyOepR.', 2, 1, 1644471785, 'default.jpg'),
('5678902', '1235437', 'Abraham', 3, 'kiju', '0895768192', 1, 'kj@gmail.com', '$2y$10$0VN2yk.s5yjBidZb7bTn3.mJp80kOQJVuHrkeqnIt8ggcPFGWZjLC', 2, 1, 1644467982, 'default.jpg'),
('5718294217', '73219394', 'Kendriflx', 2, 'Jl satria.no2', '089678902631', 1, 'kendri@gmail.com', '$2y$10$Ugf7O70F03DX6h0wWcILIuPbz8ZU3iWbEtG3X0YTpZH/vlIyZD94e', 1, 1, 1649416527, 'nigger.jpg'),
('7894561230', '78945612', 'Ajeb', 2, 'Jl.Cimahi No.2', '08957894 561', 1, '', '', 0, 0, 0, ''),
('7894565821', '45987611', 'Dardi Kur', 3, 'jl burarang', '789456120', 2, 'dardikur@gmail.com', '$2y$10$1wRtyo1pjXIyhftgxo72EOdojQfzzcgf1Bg442yL.p17kUYHJPQsi', 2, 1, 1648084034, 'default.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `siswa_access_menu`
--

CREATE TABLE `siswa_access_menu` (
  `id` int(11) NOT NULL,
  `role_id` int(11) NOT NULL,
  `menu_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `siswa_access_menu`
--

INSERT INTO `siswa_access_menu` (`id`, `role_id`, `menu_id`) VALUES
(1, 1, 1),
(2, 1, 2),
(3, 2, 2),
(4, 1, 3);

-- --------------------------------------------------------

--
-- Table structure for table `siswa_menu`
--

CREATE TABLE `siswa_menu` (
  `id` int(11) NOT NULL,
  `menu` varchar(128) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `siswa_menu`
--

INSERT INTO `siswa_menu` (`id`, `menu`) VALUES
(1, 'Admin'),
(2, 'User'),
(3, 'Menu'),
(4, 'da'),
(5, '123'),
(6, 'da'),
(7, 'test');

-- --------------------------------------------------------

--
-- Table structure for table `siswa_sub_menu`
--

CREATE TABLE `siswa_sub_menu` (
  `id` int(11) NOT NULL,
  `menu_id` int(11) NOT NULL,
  `title` varchar(128) NOT NULL,
  `url` varchar(128) NOT NULL,
  `icon` varchar(128) NOT NULL,
  `is_active` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `siswa_sub_menu`
--

INSERT INTO `siswa_sub_menu` (`id`, `menu_id`, `title`, `url`, `icon`, `is_active`) VALUES
(1, 1, 'Dashboard', 'admin', 'fas fa-fw fa-tachometer-alt', 1),
(2, 2, 'My Profile', 'siswa', 'fas fa-fw fa-user', 1),
(3, 2, 'Edit Profile', 'user/edit', 'fas fa-fw fa-user-edit', 1),
(4, 3, 'Menu Management', 'menu', 'fas fa-fw fa-folder', 1),
(5, 3, 'SubMenu Management', 'menu/submenu', 'fas fa-fw fa-folder-open', 1),
(6, 1, 'coba ', 'coba/coba', 'fas fa-fa-twitter', 0),
(7, 2, 'test', 'test/test', 'fas fa-fa-youtube', 0);

-- --------------------------------------------------------

--
-- Table structure for table `spp`
--

CREATE TABLE `spp` (
  `id_spp` int(11) NOT NULL,
  `tahun` int(11) NOT NULL,
  `nominal` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `spp`
--

INSERT INTO `spp` (`id_spp`, `tahun`, `nominal`) VALUES
(1, 2022, 450000),
(2, 2022, 500000),
(3, 2022, 400000);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `kelas`
--
ALTER TABLE `kelas`
  ADD PRIMARY KEY (`id_kelas`);

--
-- Indexes for table `pembayaran`
--
ALTER TABLE `pembayaran`
  ADD PRIMARY KEY (`id_pembayaran`),
  ADD KEY `id_petugas` (`id_petugas`),
  ADD KEY `id_spp` (`id_spp`),
  ADD KEY `jumlah_bayar` (`jumlah_bayar`),
  ADD KEY `nisn` (`nisn`);

--
-- Indexes for table `petugas`
--
ALTER TABLE `petugas`
  ADD PRIMARY KEY (`id_petugas`);

--
-- Indexes for table `siswa`
--
ALTER TABLE `siswa`
  ADD PRIMARY KEY (`nisn`),
  ADD KEY `id_kelas` (`id_kelas`),
  ADD KEY `id_spp` (`id_spp`);

--
-- Indexes for table `siswa_access_menu`
--
ALTER TABLE `siswa_access_menu`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `siswa_menu`
--
ALTER TABLE `siswa_menu`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `siswa_sub_menu`
--
ALTER TABLE `siswa_sub_menu`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `spp`
--
ALTER TABLE `spp`
  ADD PRIMARY KEY (`id_spp`),
  ADD KEY `tahun` (`tahun`),
  ADD KEY `nominal` (`nominal`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `siswa_access_menu`
--
ALTER TABLE `siswa_access_menu`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `siswa_menu`
--
ALTER TABLE `siswa_menu`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `siswa_sub_menu`
--
ALTER TABLE `siswa_sub_menu`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `pembayaran`
--
ALTER TABLE `pembayaran`
  ADD CONSTRAINT `pembayaran_ibfk_1` FOREIGN KEY (`id_petugas`) REFERENCES `petugas` (`id_petugas`),
  ADD CONSTRAINT `pembayaran_ibfk_2` FOREIGN KEY (`id_spp`) REFERENCES `siswa` (`id_spp`),
  ADD CONSTRAINT `pembayaran_ibfk_3` FOREIGN KEY (`nisn`) REFERENCES `siswa` (`nisn`);

--
-- Constraints for table `siswa`
--
ALTER TABLE `siswa`
  ADD CONSTRAINT `siswa_ibfk_1` FOREIGN KEY (`id_spp`) REFERENCES `spp` (`id_spp`),
  ADD CONSTRAINT `siswa_ibfk_2` FOREIGN KEY (`id_kelas`) REFERENCES `kelas` (`id_kelas`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
